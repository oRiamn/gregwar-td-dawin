<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/shows", name="shows")
     * @Template()
     */
    public function showsAction(Request $request)
    {
        $db = $this->getDoctrine()->getManager();

        $listUser = $db->getRepository('AppBundle:TVShow')->findByPage(
            $request->query->getInt('page', 1),
            5
        );

        return [
            'shows' => $listUser
        ];

        /*
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:TVShow');

        return [
            'shows' => $repo->findAll()
        ];
        */
    }



    /**
     * @Route("/show/{id}", name="show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:TVShow');

        return [
            'show' => $repo->find($id)
        ];        
    }

    /**
     * @Route("/calendar", name="calendar")
     * @Template()
     */
    public function calendarAction()
    {
        $db = $this->getDoctrine()->getManager();
        $episodes =  $db->getRepository('AppBundle:Episode')->findComingSoon(5);

        return [
        'episodes' => $episodes

        ];
    }

    /**
     * @Route("/search", name="search")
     * @Template()
     */
    public function search(Request $request)
    {
        $search = $request->request->get('search');

        $db = $this->getDoctrine()->getManager();
        $listShows = $db->getRepository('AppBundle:TVShow')->searchByKeyword(
            array_unique(explode(' ', $search))
        );

        return [
            'shows' => $listShows,
            'search' => $search
        ];
    }

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        return [];
    }
}
